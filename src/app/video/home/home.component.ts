import {Component, OnInit} from '@angular/core';
import {VideoService} from "../../video.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  videos: any;
  searchQuery: any = '';
  searchResults: any = [];
  toggleBtn: boolean = false;
  darkModeText: string = 'Light Mode';
  darkModeTextClass: string = 'text-white';
  darkModeColor: string = '#000'
  darkMode: any;
  textColor: any;

  constructor(private videoService: VideoService, private router: Router) {
  }

  ngOnInit(): void {
    this.videoService.darkModeColor.subscribe(res => {
      this.darkMode = res;
    })

    this.videoService.textColor.subscribe(res => {
      this.textColor = res;
    })

    this.videoService.toggleButton.subscribe(res => {
      this.toggleBtn = res;
    })

    this.getVideo();
  }

  getVideo() {
    this.videoService.getVideos().subscribe(
      (data) => {
        // I concat here to increase the number of videos
        this.videos = data.concat(data);
      },
      (error) => {
        console.log('Error fetching data:', error);
      }
    );
  }

  getVideoURL(video: any) {
    localStorage.setItem('nowPlaying', video?.videoUrl);
    localStorage.setItem('videoDescription', video?.description);
    localStorage.setItem('videoTitle', video?.title);
    localStorage.setItem('views', video?.views);
    localStorage.setItem('subscriber', video?.subscriber);
    this.router.navigate(['/now-playing']);
  }

  resizeTitle(title: string) {
    let finalTitle = ''
    if (title?.length > 24) {
      finalTitle = title.slice(0, 24);
      finalTitle = finalTitle + '...'
    } else {
      finalTitle = title;
    }
    return finalTitle;
  }

  onClickSearch() {
    this.searchResults = [];
    let videoListLength = this.videos?.length;
    for (let i = 0; i < videoListLength; i++) {
      if (this.videos[i].title.toLowerCase().includes(this.searchQuery.toLowerCase()) || this.videos[i].description.toLowerCase().includes(this.searchQuery.toLowerCase())) {
        this.searchResults.push(this.videos[i]);
      }
    }
    this.videos = this.searchResults;

    if (this.searchQuery == '') {
      this.getVideo();
    }

  }

  onChangeToggle() {
    if (this.toggleBtn) {
      this.darkModeTextClass = 'text-black';
      this.darkModeText = 'Dark Mode';
      this.darkModeColor = '#fff'
      this.videoService.darkModeColor.next('#fff');
      this.videoService.textColor.next('#000');
      this.videoService.toggleButton.next(true);
    } else {
      this.darkModeTextClass = 'text-white';
      this.darkModeText = 'Light Mode';
      this.darkModeColor = '#000'
      this.videoService.darkModeColor.next('#000');
      this.videoService.textColor.next('#fff');
      this.videoService.toggleButton.next(false);
    }
  }
}
