import {Component, OnInit} from '@angular/core';
import {VideoService} from "../../video.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-now-playing',
  templateUrl: './now-playing.component.html',
  styleUrls: ['./now-playing.component.css']
})
export class NowPlayingComponent implements OnInit {

  videoUrl: any = '';
  videoDescription: any = '';
  videoTitle: any = '';
  views: any = '';
  subscriber: any = ''

  videos: any;
  searchedVideos: any;
  showVideo: boolean = true;

  searchResults: any = [];
  searchQuery: any = '';
  toggleBtn: boolean = false;
  darkModeTextClass: string = 'text-white';
  darkModeText: string = 'Light Mode';
  darkModeColor: string = '#000';
  favouriteIconColor: string = '#fff';
  textColor: any;

  constructor(private videoService: VideoService, private router: Router) {
  }

  ngOnInit(): void {
    this.videoService.textColor.subscribe(res => {
      this.textColor = res;
    });

    this.videoUrl = localStorage.getItem('nowPlaying');
    this.videoTitle = localStorage.getItem('videoTitle');
    this.videoDescription = localStorage.getItem('videoDescription');
    this.views = localStorage.getItem('views');
    this.subscriber = localStorage.getItem('subscriber');
    this.getVideo();
    this.getSearchedVideo();
  }

  getVideo() {
    this.videoService.getVideos().subscribe(
      (data) => {
        // I concat here to increase the number of videos
        this.videos = data.concat(data);
      },
      (error) => {
        console.log('Error fetching data:', error);
      }
    );
  }

  getSearchedVideo() {
    this.videoService.getVideos().subscribe(
      (data) => {
        // I concat here to increase the number of videos
        this.searchedVideos = data.concat(data);
      },
      (error) => {
        console.log('Error fetching data:', error);
      }
    );
  }

  resizeTitle(title: string) {
    let finalTitle = ''
    if (title?.length > 24) {
      finalTitle = title.slice(0, 24);
      finalTitle = finalTitle + '...'
    } else {
      finalTitle = title;
    }
    return finalTitle;
  }

  resizeTitleOfSearchedVideos(title: string) {
    let finalTitle = ''
    if (title?.length > 17) {
      finalTitle = title.slice(0, 17);
      finalTitle = finalTitle + '...'
    } else {
      finalTitle = title;
    }
    return finalTitle;
  }

  getVideoURL(video: any) {
    this.showVideo = false;
    // set video details
    localStorage.setItem('nowPlaying', video?.videoUrl);
    localStorage.setItem('videoDescription', video?.description);
    localStorage.setItem('videoTitle', video?.title);
    localStorage.setItem('views', video?.views);
    localStorage.setItem('subscriber', video?.subscriber);

    // get video details
    this.videoUrl = localStorage.getItem('nowPlaying');
    this.videoTitle = localStorage.getItem('videoTitle');
    this.videoDescription = localStorage.getItem('videoDescription');
    this.views = localStorage.getItem('views');
    this.subscriber = localStorage.getItem('subscriber');

    setInterval(() => {
      this.showVideo = true;
    }, 50);

    this.shuffleArray(this.videos);
  }

  shuffleArray(array: any) {
    for (let i = array.length - 1; i > 0; i--) {
      // Generate a random index between 0 and i (inclusive)
      const randomIndex = Math.floor(Math.random() * (i + 1));

      // Swap the elements at randomIndex and i
      const temp = array[i];
      array[i] = array[randomIndex];
      array[randomIndex] = temp;
    }
  }

  onClickSearch() {
    this.searchResults = [];
    let videoListLength = this.searchedVideos?.length;
    for (let i = 0; i < videoListLength; i++) {
      if (this.searchedVideos[i].title.toLowerCase().includes(this.searchQuery.toLowerCase()) || this.searchedVideos[i].description.toLowerCase().includes(this.searchQuery.toLowerCase())) {
        this.searchResults.push(this.searchedVideos[i]);
      }
    }
    this.searchedVideos = this.searchResults;

    if (this.searchQuery == '') {
      this.getSearchedVideo();
    }
  }

  onChangeToggle() {
    if (this.toggleBtn) {
      this.darkModeTextClass = 'text-black';
      this.darkModeText = 'Dark Mode';
      this.darkModeColor = '#fff'
      this.videoService.textColor.next('#000');
      this.videoService.darkModeColor.next('#fff');
      this.videoService.toggleButton.next(true);
    } else {
      this.darkModeTextClass = 'text-white';
      this.darkModeText = 'Light Mode';
      this.darkModeColor = '#000';
      this.videoService.textColor.next('#fff');
      this.videoService.darkModeColor.next('#000');
      this.videoService.toggleButton.next(false);
    }
  }

  changeColor() {
    if (this.favouriteIconColor == '#fff') {
      this.favouriteIconColor = '#FF0000';
    } else if (this.favouriteIconColor == '#FF0000') {
      this.favouriteIconColor = '#fff';
    }
  }
}
