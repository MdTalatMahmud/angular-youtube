import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {NowPlayingComponent} from "./now-playing/now-playing.component";

const routes: Routes = [
  {path:'', component: HomeComponent},
  {path:'now-playing', component: NowPlayingComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideoRoutingModule { }
