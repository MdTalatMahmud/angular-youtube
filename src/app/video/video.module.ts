import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VideoRoutingModule } from './video-routing.module';
import { HomeComponent } from './home/home.component';
import { NowPlayingComponent } from './now-playing/now-playing.component';
import {FormsModule} from "@angular/forms";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatIconModule} from "@angular/material/icon";


@NgModule({
  declarations: [
    HomeComponent,
    NowPlayingComponent
  ],
    imports: [
        CommonModule,
        VideoRoutingModule,
        FormsModule,
        MatSlideToggleModule,
        MatIconModule
    ]
})
export class VideoModule { }
