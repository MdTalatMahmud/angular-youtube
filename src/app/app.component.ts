import {Component, OnInit} from '@angular/core';
import {VideoService} from "./video.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'angular-youtube';
  darkMode: any;


  constructor(private videoService: VideoService) {}

  ngOnInit() {
    this.videoService.darkModeColor.subscribe(res => {
      this.darkMode = res;
    })
    // this.videoService.getVideos().subscribe(
    //   (data) => {
    //     this.videos = data;
    //     console.log(this.videos);
    //   },
    //   (error) => {
    //     console.log('Error fetching data:', error);
    //   }
    // );
  }
}
