import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  private apiUrl = 'https://gist.githubusercontent.com/poudyalanil/ca84582cbeb4fc123a13290a586da925/raw/14a27bd0bcd0cd323b35ad79cf3b493dddf6216b/videos.json';
  darkModeColor: Subject<any> = new Subject<any>();
  textColor: Subject<any> = new Subject<any>();
  toggleButton: Subject<any> = new Subject<any>();

  constructor(private http: HttpClient) {
  }

  getVideos(): Observable<any> {
    return this.http.get<any>(this.apiUrl);
  }
}
